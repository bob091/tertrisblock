from figure import Figure

class GameOperation:
    def __init__(self, heigh, witdt, field):
        """Konstruktor bez parametrów"""
        self.heigh = heigh
        self.width = witdt
        self.field = field

    def new_figure(self):
        self.figure = Figure(3, 0)

    def _intersects(self):
        intersection = False
        for i in range(4):
            for j in range(4):
                if i * 4 + j in self.figure.image():
                    if i + self.figure.y > self.heigh - 1 or \
                            j + self.figure.x > self.width - 1 or \
                            j + self.figure.x < 0 or \
                            self.field[i + self.figure.y][j + self.figure.x] > 0:
                        intersection = True
        return intersection

    def _break_lines(self):
        lines = 0
        for i in range(1, self.heigh):
            zeros = 0
            for j in range(self.width):
                if self.field[i][j] == 0:
                    zeros += 1
            if zeros == 0:
                lines += 1
                for i1 in range(i, 1, -1):
                    for j in range(self.width):
                        self.field[i1][j] = self.field[i1 - 1][j]
        self.score += lines ** 2

    def go_space(self):
        while not self._intersects():
            self.figure.y += 1
        self.figure.y -= 1
        self._freeze()

    def go_down(self):
        self.figure.y += 1
        if self._intersects():
            self.figure.y -= 1
            self._freeze()

    def _freeze(self):
        for i in range(4):
            for j in range(4):
                if i * 4 + j in self.figure.image():
                    self.field[i + self.figure.y][j + self.figure.x] = self.figure.color_block
        self._break_lines()
        self.new_figure()
        if self._intersects():
            self.state = "gameover"

    def go_side(self, dx):
        old_x = self.figure.x
        self.figure.x += dx
        if self._intersects():
            self.figure.x = old_x

    def rotate(self):
        old_rotation = self.figure.rotation
        self.figure.rotate()
        if self._intersects():
            self.figure.rotation = old_rotation

    def write_best_score(self, score):
        if score > self.best_score:
            self.best_score = score

