import random

class Figure:
    """Klasa reprezentująca figurę (klocka)"""
    #Lista klocków
    blocs =[
        [[1, 5, 9, 13], [4, 5, 6, 7]],
        [[1, 2, 5, 9], [0, 4, 5, 6], [1, 5, 9, 8], [4, 5, 6, 10]],
        [[1, 2, 6, 10], [5, 6, 7, 9], [2, 6, 10, 11], [3, 5, 6, 7]],
        [[1, 4, 5, 6], [1, 4, 5, 9], [4, 5, 6, 9], [1, 5, 6, 9]],
        [[1, 2, 5, 6]],
    ]

    #Lista kolorów dla klocków
    colors = [
        (0, 0, 0),
        (120, 37, 179),
        (100, 179, 179),
        (80, 34, 22),
        (80, 137, 22),
        (181, 34, 22),
        (180, 34, 122),
    ]

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.type_block = random.randint(0, len(self.blocs) - 1)
        self.color_block = random.randint(0, len(self.colors) - 1)
        self.rotation = 0

    def image(self):
        """metoda zwracająca klocka"""
        return self.blocs[self.type_block][self.rotation]

    def rotate(self):
        self.rotation = (self.rotation + 1) % len(self.blocs[self.type_block])
