import pygame
import random

import gameOperation
from figure import Figure
from gameOperation import GameOperation

class Tetris(GameOperation):
    level = 2
    score = 0
    state = "start"
    field = []
    height = 0
    width = 0
    x = 100
    y = 60
    zoom = 20
    figure = None

    def __init__(self, height, width, score):
        super().__init__(height, width, score)
        self.height = height
        self.width = width
        self.field = []
        self.score = 0
        self.best_score = score
        self.state = "start"
        for i in range(height):
            new_line = []
            for j in range(width):
                new_line.append(0)
            self.field.append(new_line)



# Inicjalizacja gry
pygame.init()

# definicja bazowych kolorów
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GRAY = (128, 128, 128)

size = (900, 600)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Tetris")

# Loop until the user clicks the close button.
done = False
clock = pygame.time.Clock()
fps = 25
game = Tetris(25, 15, 0)
counter = 0

pressing_down = False

while not done:
    if game.figure is None:
        game.new_figure()
    counter += 1
    if counter > 100000:
        counter = 0

    if counter % (fps // game.level // 2) == 0 or pressing_down:
        if game.state == "start":
            game.go_down()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                game.rotate()
            if event.key == pygame.K_DOWN:
                pressing_down = True
            if event.key == pygame.K_LEFT:
                game.go_side(-1)
            if event.key == pygame.K_RIGHT:
                game.go_side(1)
            if event.key == pygame.K_SPACE:
                game.rotate()#game.go_space()
            if event.key == pygame.K_ESCAPE:
                game.__init__(25, 15, game.best_score)
            if event.key == pygame.K_q:
                pygame.quit()

    if event.type == pygame.KEYUP:
            if event.key == pygame.K_DOWN:
                pressing_down = False

    screen.fill(WHITE)

    for i in range(game.height):
        for j in range(game.width):
            pygame.draw.rect(screen, GRAY, [game.x + game.zoom * j, game.y + game.zoom * i, game.zoom, game.zoom], 1)

            if game.field[i][j] > 0:
                pygame.draw.rect(screen, Figure.colors[game.field[i][j]],
                                 [game.x + game.zoom * j + 1, game.y + game.zoom * i + 1, game.zoom - 2, game.zoom - 1])

    if game.figure is not None:
        for i in range(4):
            for j in range(4):
                p = i * 4 + j
                if p in game.figure.image():
                    pygame.draw.rect(screen, Figure.colors[game.figure.color_block],
                                     [game.x + game.zoom * (j + game.figure.x) + 1,
                                      game.y + game.zoom * (i + game.figure.y) + 1,
                                      game.zoom - 2, game.zoom - 2])


    font = pygame.font.SysFont('Calibri', 25, True, False)
    font1 = pygame.font.SysFont('Calibri', 65, True, False)
    text = font.render("Score: " + str(game.score), True, BLACK)
    text_best = font.render('Najlepszy wynik: ' + str(game.best_score), True, BLACK)
    #text instrukcji
    text_instruction = font.render('Intrukcja : ', True, BLACK)
    text_inst2 = font.render('Strzałka w gurę / Spacja - obrót', True, BLACK)
    text_inst3= font.render('Strzałka w prawo(->) - ruch w prawo', True, BLACK)
    text_inst4 = font.render('Strzałka w lewo(<-) - ruch w lewo', True, BLACK)
    text_inst5 = font.render('Strzałka w dół - przyspieszenie ', True, BLACK)
    text_inst6 = font.render('ESC - Nowa gra', True, BLACK)
    text_inst7 = font.render('q - Zamknij', True, BLACK)

    text_game_over = font1.render("Game Over", True, (255, 125, 0))
    text_game_over1 = font1.render("Naciśnij ESC aby grać dalej", True, (255, 215, 0))

    screen.blit(text, [500, 20])
    screen.blit(text_best, [500, 60])
    # Wyświetlanie tekstu instrukcji
    screen.blit(text_instruction, [500, 100])
    screen.blit(text_inst2, [500, 120])
    screen.blit(text_inst3, [500, 140])
    screen.blit(text_inst4, [500, 160])
    screen.blit(text_inst5, [500, 180])
    screen.blit(text_inst6, [500, 200])
    screen.blit(text_inst7, [500, 220])

    if game.state == "gameover":
        screen.blit(text_game_over, [20, 200])
        screen.blit(text_game_over1, [25, 265])
        game.write_best_score(game.score)


    pygame.display.flip()
    clock.tick(fps)

pygame.quit()
